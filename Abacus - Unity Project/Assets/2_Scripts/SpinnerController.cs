﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpinnerController : MonoBehaviour
{
    public float rotSpeed;
    public float speedDecay;
    public Image arrow;
    public float minSpeed;
    public float maxSpeed;
    public float zRotation;
    public RectTransform rect;
    public float[] anglesMax;
    public float[] anglesMin;
    public float angleAv;
    public float avAdjust;
    public Color indiColour;
    public Color arrowCol;
    Renderer rend;

    void Start()
    {
        // SetColour();
    }

    void Update()
    {
        Roatation();

        if(rotSpeed >= 0.5f)
        {
            Decay();
        }

        if(rotSpeed <= 0.5f)
        {
            AngleStop();
        }
    }

    public void Spin()
    {
        this.rotSpeed = Random.Range(minSpeed, maxSpeed);
        // arrowCol.a = 0f;
        // arrow.color = arrowCol;
    }

    void SetColour()
    {
        // arrowCol = arrow.color;
    }

    void Roatation()
    {
        zRotation = rect.eulerAngles.z;
        transform.Rotate(0,0,rotSpeed);
    }

    void Decay()
    {
        this.rotSpeed *= speedDecay;
    }

    void AngleStop()
    {
        for(int i = 0; i < anglesMax.Length; i++)
        {
            if(zRotation >= anglesMin[i] && zRotation <= anglesMax[i])
            {
                this.rotSpeed = 0;
                if(this.rect.eulerAngles.z > anglesMax[i])
                {
                    angleAv = anglesMax[i] - anglesMin[i];
                    avAdjust = Mathf.Round(rect.eulerAngles.z - angleAv);
                    RectChange();                   
                }

                if(this.rect.eulerAngles.z < anglesMin[i])
                {
                    angleAv = anglesMax[i] - anglesMin[i];
                    avAdjust = Mathf.Round(rect.eulerAngles.z + angleAv); 
                    RectChange();
                }
                // arrowCol.a = 1f;
                // arrow.color = arrowCol;
            }
        }
    }

    void RectChange()
    {
        this.rect.eulerAngles = new Vector3(0,0,avAdjust); 
    }
}
