using System;
using UnityEngine;

namespace TwinklAuthenticationGateway
{   
    public abstract partial class iTransitionPanel : MonoBehaviour
    { 
        public enum ActionOnStart
        {
            Nothing,
            Hide,
            Show
        }
    
        [SerializeField] internal ActionOnStart onStartAction;
        [Range(0.1f, 3.0f)]
        [SerializeField] internal float defaultAnimationTime = 1.0f;

        public virtual void Start()
        {
            switch (onStartAction)
            {
                case ActionOnStart.Nothing:
                    return;
                case ActionOnStart.Hide:
                    CoverScreen();
                    return;
                case ActionOnStart.Show:
                    UncoverScreen();
                    break;
            }
        }

        public virtual void UncoverScreen(Action _onComplete = null)
        {
        }

        public virtual void CoverScreen(Action _onComplete = null)
        {
        }
    }

}