﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineDisplay : MonoBehaviour
{
    public List<GameObject> lineControllers = new List<GameObject>();
    public GameObject rackArea;
    public int activeLines;
    public Text displayText;
    public ScoreController scScript;
    
    // Start is called before the first frame update
    void Start()
    {
        StartUpSequence();
        
        activeLines = 2;
        displayText.text = "2";
        EditLines();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartUpSequence()
    {
        activeLines = 4;
        rackArea.GetComponent<VerticalLayoutGroup>().enabled = true;
        EditLines();
    }

    public void EditLines()
    {
        foreach(GameObject i in lineControllers)
        {
            i.SetActive(false);
        }

        for(int i=0;i<activeLines;i++)
        {
            lineControllers[i].SetActive(true);
        }

        if(activeLines > 4)
        {
            rackArea.GetComponent<VerticalLayoutGroup>().enabled = true;
        }
        else
        {
            rackArea.GetComponent<VerticalLayoutGroup>().enabled = false;
        }
    }

    public void editButton(int x)
    {
        switch(x)
        {
            case 1:
                if(activeLines != 10)
                {
                    activeLines++;
                    displayText.text = activeLines.ToString();
                }
            break;

            case -1:
                if(activeLines != 1)
                {
                    activeLines--;
                    displayText.text = activeLines.ToString();
                }
            break;
        }
        EditLines();
    }

    public void ResetAllLines()
    {
        foreach(GameObject i in lineControllers)
        {
            i.transform.GetChild(0).gameObject.GetComponent<MoveBead>().ResetBeads();
        }
    }
}
