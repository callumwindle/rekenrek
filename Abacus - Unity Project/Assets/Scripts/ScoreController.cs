﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public GameObject  toggleCountersText, toggleTotalsText;
    public List<GameObject> counterList = new List<GameObject>();
    private Text totalLeftText, totalRightText;
    private string countTotalText;
    private int countTotal, sumOf;
    private GameObject currCounter;
    public bool isLeftCovered, isRightCovered, showTotals, showCounter;
    
    // Start is called before the first frame update
    void Start()
    { 
        showCounter = false;
        showTotals = false;

        ToggleCounters();
        ToggleTotals();
    }

    public void ToggleCounters()
    {
        if(showCounter)
        {
            for(int i=0; i<counterList.Count; i++)
            {
                currCounter = counterList[i];
                currCounter.SetActive(false);
                
            }
            showCounter = !showCounter;
            toggleCountersText.GetComponent<Text>().text = "Show Counters";           
        }
        else
        {
            for(int i=0; i<counterList.Count; i++)
            {
                currCounter = counterList[i];
                currCounter.SetActive(true);
                
            }
            showCounter = !showCounter;
            toggleCountersText.GetComponent<Text>().text = "Hide Counters";
        }
    }

    public void ToggleTotals()
    {
        
    }
    
}
